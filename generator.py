import itertools
import numpy as np
from fractions import Fraction
import pandas as pd

# base frequency
f=500

# minimum frequency
f_min = 20

# largest required divider to reach the minimum frequency
max_div = int(f/f_min)

# max unmber fo stim blocks
sbs = 6

#%% 
primes = [2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97]
class Node():
    """
    object defining a knot of the tree.
    dividers: list of dividers of this node and the ones above 
    """
    def __init__(self, div, level, dividers):
        self.level = level
        self.dividers = dividers
        
        self.list = []
        
        # try to make a next node for each prime divider
        for next_divider in primes:
            # verify that the next node does not exceed the max divider
            if self.dividers[-1]*next_divider<=max_div:
                # create the node
                self.list.append(Node(next_divider, self.level+1, self.dividers + [self.dividers[-1]*next_divider]))
                
            
    def show(self):
        # print("\t"*self.level + f"dividers: {self.dividers}")
        print("\t"*self.level + f"frequencies: {[f/divider for divider in self.dividers]}")
        for node in self.list:
            node.show()
    
    def get_frequencies(self, combo):
        """
        calculate the frequency for a given combination
        """
        max_div = max(combo)
        low_freq = int(round(f/max_div))
        
        freqs = [int(low_freq*max_div/i) for i in combo]
        
        return freqs
    
    def get_delays(self, combo):
        """
        calculate the delays for a given combination
        """
        # print(f"Frequencies: {[int(base_frequency/x) for x in combo]} Hz")
        
        timeline = [0 for i in range(max(combo))]
        starts = []
        for div in combo:
            start = np.where(np.array(timeline) == 0)[0][0]
            starts.append(start)
            for t in range(max(combo)):
                if not (t-start)%div:
                    timeline[t]+=1
        return [int(n*1e3/f) for n in starts]


    def makes_collision(self, freqs, delays, combination):
        """
        calculate the delays for a given combination
        """
        base_freq = freqs[0]*combination[0]
        t_delay = max(delays) #[ms]
        t_grid = max(delays)*500/base_freq #[ms]
        delta = abs(t_delay-t_grid)
        
        if delta>=0.35:
            input()
        
        print(f"base_freq: {base_freq}, t_delay: {t_delay}, t_grid: {t_grid}, delta: {delta}") 
        return delta>=0.350

    def get_combinations(self, sb, solutions):
        combinations = list(itertools.combinations_with_replacement(self.dividers,sb))
        for combination in combinations:
            if self.dividers[-1] in combination and sum(Fraction(1, d) for d in combination)<=1:
                delays = self.get_delays(combination)
                freqs = self.get_frequencies(combination)
                if max(delays)<= 20 and not self.makes_collision(freqs, delays, combination):
                    solutions.append({"div": combination,
                                      "freq": self.get_frequencies(combination),
                                      "delays": self.get_delays(combination)})                
        
        for node in self.list:
            node.get_combinations(sb, solutions)

#%% build the tree and show
tree = Node(1, 0, [1])
tree.show()

#%% extract results from the tree
results = []
dfs = []
for sb in range(1, sbs+1):
    print(f"\nCombinations for {sb} stim blocks")
    solutions = []
    tree.get_combinations(sb, solutions)
    
    # solutions = []
    # for freq, delay in zip(frequencies, delays):
    #     if max(delay)<=20000:
    #         solutions.append(frequencies+delays)
            
    # results.append(sorted(set(solutions)))
    results.append((solutions))
    # print(results[-1])


#%% export to excel

# freqs = [[[round(f/divider, 2) for divider in combo]+get_delays(combo, f) for combo in sb]for sb in results]
# dfs = [pd.DataFrame(sb, columns=[f'Frequency #{i+1} [Hz]' for i in range(len(sb[0]))]+[f'Delay #{i+1} [us]' for i in range(len(sb[0]))]) for sb in freqs]

with pd.ExcelWriter('output.xlsx') as writer:  
    for i in range(sbs):
        export = [solution["freq"]+solution["delays"] for solution in results[i]]
        
        export = set(tuple(row) for row in export)
        
        
        df = pd.DataFrame(sorted(export, reverse=True), columns=[f'Frequency #{k+1} [Hz]' for k in range(i+1)]+[f'Delay #{k+1} [ms]' for k in range(i+1)])
        df.to_excel(writer, sheet_name=f'{i+1} Stim blocs')

#%% find optimal solution
# request = [24, 57, 33]
# request = [350, 53, 48]

# dividers = sorted([max(request)/x for x in request])
# perf = []
# for combo in results[len(request)-1]:
    
#     combo = [x/min(combo) for x in combo]
    
#     perf.append(sum([abs(x-y) for x,y in zip(dividers, combo)]))

# index = np.argmin(perf)

# solution = results[len(request)-1][index]

# print(f"Request: {sorted(request, reverse=True)} Hz")
# print("solution: ", solution)

# f = min(solution) * max(request)

# print("f: ", f)

# calculate(solution)

# #%% verify combinations for colision
# # number of slots to test the colisions
# duration = 10000
# def verify(combo):
#     timeline = [0 for i in range(duration)]
#     starts = []
#     for div in combo:
#         start = np.where(np.array(timeline) == 0)[0][0]
#         assert start<max(combo)
#         starts.append(start)
#         for t in range(duration):
#             if not (t-start)%div:
#                 timeline[t]+=1
#     # print(f"start positions: {starts}")
#     return max(timeline)==1

# # test all results
# for num_sbs in results:
#     for combo in num_sbs:
#         assert verify(combo)
# print("all results verified for overlap")












#|   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   |   






















